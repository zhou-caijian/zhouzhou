package com.example.teachdemo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

public class ChronoActivity extends AppCompatActivity {

    Chronometer chronometer;
    Button btnStart,btnStop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chrono);
        chronometer = findViewById(R.id.chronometer);
        btnStart = findViewById(R.id.btnStart);
        btnStop = findViewById(R.id.btnStop);
        chronometer.setCountDown(true);
        chronometer.setBase(SystemClock.elapsedRealtime() + 60000);
        chronometer.start();
        btnStart.setEnabled(false);
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                btnStart.setText(chronometer.getText());
                if(chronometer.getText().equals("00:00")){
                    chronometer.stop();
                    btnStart.setText("启动");
                    btnStart.setEnabled(true);
                }
            }
        });
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ChronoActivity.this, "aaaaa", Toast.LENGTH_SHORT).show();
            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chronometer.stop();
            }
        });

    }
}
