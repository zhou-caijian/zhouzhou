package com.example.teachdemo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Random;

public class ProgressBarActivity extends AppCompatActivity {

    ProgressBar progressBar;
    Button btnSet,btnStop;
    int value = 0;
    boolean isStop = false;
    Random random = new Random();
    TextView tvMsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);
        progressBar = findViewById(R.id.progressBar);
        btnSet = findViewById(R.id.btnSet);
        btnStop = findViewById(R.id.btnStop);
        tvMsg = findViewById(R.id.tvMsg);
        progressBar.setProgress(value);

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.v("ran:","" + (int)(Math.random() * 10));
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(!isStop){

                            value = value + random.nextInt(10);

                            /*
                            if(value >= 100){
                                progressBar.setProgress(100);
                                tvMsg.setText("value:" + 100);
                                break;
                            }*/
                            if(value >= 100){
                                value = 100;
                                isStop = true;
                            }
                            Log.v("ran:","" + random.nextInt(10) + "value:" + value);
                            tvMsg.setText("value:" + value);//老版本会出问题
                            progressBar.setProgress(value);//老版本会出问题
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }).start();
            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.v("stop:","stop");
                isStop = true;
                progressBar.setProgress(value);

            }
        });

    }
}
