package com.example.teachdemo4;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btnSubmit;
    ImageButton imgbtnGet;
    EditText etPhone,etBirth;
    RadioButton rbtnMale,rbtnFemale;
    RadioGroup rgSex;
    CheckBox chkJava,chkFront,chkPython;
    TextView tvMsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uidemo);
        //java变量与view（组件）关联

        btnSubmit = findViewById(R.id.btnSubmit);
        imgbtnGet = findViewById(R.id.imgbtnGet);
        etPhone = findViewById(R.id.etPhone);

        etBirth = findViewById(R.id.etBirth);
        rbtnMale = findViewById(R.id.rbtnMale);
        rbtnFemale = findViewById(R.id.rbtnFemale);

        rgSex = findViewById(R.id.rgSex);
        chkJava = findViewById(R.id.chkJava);
        chkFront = findViewById(R.id.chkFront);
        chkPython = findViewById(R.id.chkPython);
        tvMsg = findViewById(R.id.tvMsg);
        etBirth.setEnabled(false);
        rgSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                System.out.println("id:"+radioGroup.getCheckedRadioButtonId());
                if(radioGroup.getCheckedRadioButtonId()==R.id.rbtnMale){
                    System.out.println("性别：男");
                }
                if(radioGroup.getCheckedRadioButtonId()==R.id.rbtnFemale){
                    System.out.println("性别：女");
                }
            }
        });
        chkJava.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                System.out.println("课程:Java");
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvMsg.setText("msg");//在组件上展示信息
                //后台打印信息
                System.out.println("msg");
                Log.v("msg2","msg3");
                String strSex = "";
                if(rbtnMale.isChecked()){
                    strSex = "男";
                }
                if(rbtnFemale.isChecked()){
                    strSex = "女";
                }
                //String strCourse = "";
                StringBuffer sb = new StringBuffer();
                if(chkJava.isChecked()){
                    //strCourse += "java";
                    sb.append("java ");
                }
                if(chkFront.isChecked()){
                    //strCourse += "前端";
                    sb.append("前端 ");
                }
                if(chkPython.isChecked()){
                    //strCourse += "python";
                    sb.append("python ");
                }
                //消息提示
                Toast.makeText(MainActivity.this, "手机号："+ etPhone.getText() +"性别：" + strSex+" 课程："+sb, Toast.LENGTH_SHORT).show();
            }
        });

        imgbtnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog pickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        etBirth.setText("" +year + "-" + (month + 1) + "-" + day);
                    }
                },2020,9,14);

                pickerDialog.show();
            }
        });

    }
}
